# Sherpa Website
The sherpa website is powered by [sphinx >= 2.2.0](http://www.sphinx-doc.org/en/master/).

A special extension (see `extensions/gitlab-tags.py`) generates the
changelog page via the `gitlab-tags` reST directive (see `source/changelogs.rst`).

## Creation of new Pages
To create a new page, simply create a markdown (`.md`) or a reST
(`.rst`) document in the `source/pages` folder. It will automatically be
included.

For information about cross-referencing and the like, see the [sphinx
docs](https://www.sphinx-doc.org/en/master/contents.html) and consult
[this
document](https://recommonmark.readthedocs.io/en/latest/auto_structify.html)
if you are using markdown.

To create a reference label for a heading use:

`````markdown
```label
label-name
```

# Some heading
`````

## Building
 - run `pip install -r requirements.txt` to install the dependencies
 - run `make` and follow the displayed hints

## Caveats
 - to get markdown support to work right, I had to fork
   [recommonmark](https://github.com/readthedocs/recommonmark) to fix
   inline math and add reference labels


## Automatic manual integration

The manual is being rebuilt in the main [Sherpa repo's CI](https://gitlab.com/sherpa-team/sherpa/-/blob/master/.gitlab-ci.yml) upon changes. It will be deployed automatically to `sherpa-team.gitlab.io/sherpa/<branch>` where `<branch>=master, ...`. (The above is only valid for Sherpa >=3.0.0.)
