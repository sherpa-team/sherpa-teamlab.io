"""Gitlab Tags integration for Sphinx.

Author: Valentin Boettcher <hiro@protagon.space>
"""

import re
import requests
import itertools

from docutils import nodes
from docutils.parsers.rst import directives
from docutils.utils import new_document
from docutils.frontend import OptionParser

from sphinx.util.docutils import SphinxDirective

from recommonmark.parser import CommonMarkParser

class TagListDirective(SphinxDirective):
    """
    A simple directive to download an parse markdown tag release notes
    embdedding them into the document.
    """

    has_content = True
    option_spec = {
        'api-url': directives.uri,
        'project': directives.unchanged_required,
        'tag-link-text': directives.unchanged_required,
        'fulltext': directives.positive_int,
    }
    _rel_link_re = re.compile(r'\[(.*?)\]\((/.*?)\)', re.MULTILINE)
    _mail_link_re = re.compile(r'\[(.*?)\]\((.*?@.*?)\)', re.MULTILINE)
    _issue_re = re.compile(r'#([0-9]+)', re.MULTILINE)
    _mr_re = re.compile(r'\!([0-9]+)', re.MULTILINE)

    def __init__(self, *args, **kwargs):
        self._repo = ''
        self._tags = ''
        self._clean_repo_url = ''

        super().__init__(*args, **kwargs)

    def get_tag_url(self, tag):
        """
        :param tag: a tag dictonary from th gitlab api
        :returns: a url pointing to the tags page on gitlab
        :rtype: str
        """

        return self._clean_repo_url \
            + '/-/releases/' + tag['name']


    def parse_md(self, text, ident):
        """Parses markdown into a list of docutils nodes.

        :param text: the input markdown text
        :returns: nodes
        :rtype: docutils.node
        """

        parser = CommonMarkParser()
        parser.parse(text, new_document('changelogs/' + ident,
                                        settings=OptionParser(
                                            components=(CommonMarkParser,)
                                        ).get_default_values()))

        return parser.document

    def get_repo_and_tags(self):
        """Download repo and tags from the gitlab api."""

        self._repo = requests.get(self.options['api-url'] + '/projects/' \
                                  + self.options['project']) \
                                  .json()

        self._clean_repo_url = self._repo['http_url_to_repo'].replace('.git', '')

        if 'error' in self._repo:
            raise RuntimeError("Repo API error: " + self._repo['error'])

        self._tags = []

        # get the all
        for page in itertools.count(1):
            tags = requests.get(self.options['api-url'] \
                                + '/projects/' + self.options['project'] \
                                + '/repository/tags?per_page=100&page=' \
                                + str(page)).json()

            if 'error' in self._tags:
                raise RuntimeError("Repo API error: " + self._tags['error'])

            if not tags:
                break

            self._tags += tags

    def rewrite_relative_links(self, text):
        """Prepends the repo url to relative links.

        :param text: markdown input
        :returns: the altered text
        :rtype: str
        """

        return re.sub(self._rel_link_re, r'[\1](' + self._clean_repo_url \
                 + r'/\2)', text)

    def rewrite_email_links(self, text):
        """Converts bare emails in references into mailto links.

        :param text: markdown input
        :returns: the altered text
        :rtype: str
        """

        return re.sub(self._mail_link_re, r'[\1](mailto:\2)', text)

    def rewrite_issues_and_mrs(self, text):
        """Converts (!N) and (#N) into references to the respective
        merge request and issues on gitlab.

        :param text: markdown input
        :returns: the altered text
        :rtype: str
        """

        res = re.sub(self._mr_re, r'[!\1](' + self._clean_repo_url \
                     + r'/merge_requests/\1)', text)
        res = re.sub(self._issue_re, r'[#\1](' + self._clean_repo_url \
                     + r'/issues/\1)', res)

        return res

    def get_tag_ref(self, tag):
        """
        Makes a docutils reference which points to the gitlab page for
        the tag in a pragraph.
        """

        tag_link = nodes.reference(text=self.options['tag-link-text'])
        tag_link['refuri'] = self.get_tag_url(tag)

        return tag_link

    def run(self):
        """Main entry for the directive."""

        self.get_repo_and_tags()

        max_index = int(self.options['fulltext'])
        r_nodes = []

        for index, tag in enumerate(self._tags):
            # only releases
            if 'release' not in tag:
                pass

            if index < max_index:

                # fix up markdown and gitlab quirks
                rel_notes = \
                    self.rewrite_relative_links(tag['release']['description'])

                rel_notes = self.rewrite_email_links(rel_notes)
                rel_notes = self.rewrite_issues_and_mrs(rel_notes)

                # parse and append
                r_nodes += self.parse_md(rel_notes, tag['target']).children

            else:
                name = extract_first_heading(tag)
                name = name if name else tag['name']

                section = nodes.section(ids=[nodes.make_id(name)],
                                        names=[name])

                section += nodes.title(name, name)

                p = nodes.paragraph()
                p += nodes.Text(tag['message'])
                section += p

                p = nodes.paragraph()
                p += self.get_tag_ref(tag)

                section += p

                r_nodes += [section]

        return r_nodes

def extract_first_heading(tag):
    """Extracts the first heading from the tag release notes.

    :param tag: a tag dict
    :returns: the heading or None
    :rtype: str or None
    """

    headers = re.search(r'(?:#+\s*(.*)$|(.*)\n=+\s+)',
                        tag['release']['description'],
                        re.MULTILINE)


    return next(filter(lambda x: x != None, headers.groups())) if headers else None

def setup(app):
    app.add_directive('gitlab-tags', TagListDirective)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
