# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
from os.path import abspath, join, dirname
import recommonmark
from recommonmark.transform import AutoStructify

sys.path.insert(0, os.path.abspath('../extensions/'))
sys.path.insert(0, os.path.abspath('./'))
from shared_conf import *

project = 'Sherpa'

# The full version, including alpha/beta/rc tags
release = ''

extensions = [
#    'sphinx.ext.autosectionlabel',
    'recommonmark',
    'gitlab-tags'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}

html_static_path = ['_static']
html_favicon = '_static/images/favicon.ico'

master_doc = 'index'

html_context = {
    'source_prefix': 'https://gitlab.com/sherpa-team/sherpa-team.gitlab.io/blob/master/source/'
}
html_theme_options['extra_nav_links'] = {
    'Manual': 'https://sherpa-team.gitlab.io/sherpa/master/',
    'Issue Tracker': 'https://gitlab.com/sherpa-team/sherpa/issues/',
    'Git Repo': 'https://gitlab.com/sherpa-team/sherpa/',
    'Mailing List': 'https://www.hepforge.org/lists/listinfo/sherpa-announce'
}

html_css_files = [
    'css/custom.css',
]

# app setup hook
def setup(app):
    app.add_config_value('recommonmark_config', {
        #'url_resolver': lambda url: github_doc_root + url,
        'auto_toc_tree_section': 'Contents',
        'enable_math': True,
        'enable_inline_math': True,
        'enable_eval_rst': True,
    }, True)
    app.add_transform(AutoStructify)
