```label
SherpaTeam
```

Sherpa Team
===========

You can reach the Sherpa team with your questions, comments and praise
at
[sherpa\@projects.hepforge.org](mailto:sherpa@projects.hepforge.org).

Bug reports should be entered into our [issue
tracker](https://gitlab.com/sherpa-team/sherpa/issues).

If you would like to receive announcements of new Sherpa releases and
important updates, you may subscribe to our [announcement mailing
list](http://www.hepforge.org/lists/listinfo/sherpa-announce).

To discuss Sherpa usage issues with other users, there is a public
mailing list at
[http://www.hepforge.org/lists/listinfo/sherpa-discuss](http://www.hepforge.org/lists/listinfo/sherpa-discuss).

Current members
---------------

### Enrico Bothmann

-   PostDoc at at [Uni Goettingen](https://www.uni-goettingen.de/en/team/581848.html)
-   On-the-fly reweighting, CSShower, framework I/O

### Stefan Hoeche

-   ME+PS merging, NLO, HERA physics,
    [Comix](http://comix.freacafe.de), Parton Showers
-   [personal website](http://www.freacafe.de)

### Mareen Hoppe

-   Polarised cross sections
-   UFO interface
-   PhD Student at [TU Dresden](https://tu-dresden.de/mn/physik/iktp/)

### Frank Krauss

-   Overall event generation framework, ME+PS merging, underlying event,
    hadronization models
-   [personal website](http://www.ippp.dur.ac.uk/~krauss)

### Marek Schoenherr

-   Faculty at [IPPP Durham](http://www.ippp.dur.ac.uk/)
-   ME+PS merging, NLO, Photon radiation, SUSY decays

### Steffen Schumann

-   Faculty at [Uni Goettingen](https://www.uni-goettingen.de/en/team/581848.html)
-   (B)SM phenomenology, ML methods, tuning
-   [personal website](https://www.uni-goettingen.de/en/577815.html)

### Frank Siegert

-   Heavy-flavour production, prompt photon production
-   Decays, polarised cross sections
-   ATLAS support
-   Group leader at [TU Dresden](https://tu-dresden.de/mn/physik/iktp/)

Former members
--------------

### Jennifer Archibald

-   Former PhD student (2007-2011)
-   QED subtraction, massive QCD subtraction, SUSY QCD subtraction
-   now at [Carpmaels&Ransford](http://www.carpmaels.com)

### Timo Fischer

-   Former diploma student (2005-2006)
-   Exotic physics models, helicity method for spin 3/2 particles
-   now at [Uni
    Goettingen](http://www.theorie.physik.uni-goettingen.de/~fischer/)

### Tanju Gleisberg

-   Former diploma and PhD student in Dresden (2002-2007), PostDoc at
    SLAC (2007-2010)
-   Exotic physics models, phase-space integration, Catani-Seymour
    subtraction
-   now at [ISEG HV](http://www.iseg-hv.com/)

### Hendrik Hoeth

-   Former staff member (2008-2013)
-   Hadronization, Minimum Bias, Tuning

### Johannes Krause

-   Former PhD student at [TU Dresden](https://tu-dresden.de/mn/physik/iktp/arbeitsgruppen/emmy-noether-nachwuchsgruppe/)
-   Prompt photon production and isolation
-   Heavy-flavour production

### Ralf Kuhn

-   Former diploma and PhD student in Dresden (1997-2002)
-   now at [software company](mailto:ralf.kuhn@web.de)

### Silvan Kuttimalai

-   Former Master student in Goettingen, PhD student in Durham, and PostDoc at SLAC
-   BSM, Comix

### Thomas Laubrich

-   Former diploma student in Dresden (2005-2006)
-   Tau decays
-   now at [d-fine](http://www.d-fine.de/en/)

### Sebastian Liebschner

-   Former PhD student at [TU Dresden](https://tu-dresden.de/mn/physik/iktp/arbeitsgruppen/emmy-noether-nachwuchsgruppe/)
-   NLO subtraction and NLO+PS matching for coloured resonances

### Radoslaw Matyszkiewicz

-   Former postdoc in Dresden (2005-2007)
-   MHV formalism for matrix elements

### Andreas Schaelicke

-   Former diploma and PhD student in Dresden (1999-2005)
-   Parton showers, ME+PS merging
-   now at [DESY Zeuthen](http://www-zeuthen.desy.de/~dreas/)

### Jennifer Thompson

-   PhD student at [IPPP Durham](http://www.ippp.dur.ac.uk/)
-   NLO, ME+PS merging at NLO
-   now at [Heidelberg](https://www.thphys.uni-heidelberg.de)

### Jan Winter

-   Former PhD student in Dresden, PostDoc at Fermilab, CERN, MPI Munich, MSU
-   Parton Showers, NLO, hadronization models

### Korinna Zapp

-   PostDoc [CERN](http://ph-dep-th.web.cern.ch/ph-dep-th/)
-   Minimum Bias, Parton Showers

