```label
theses
```

Theses
======

Within our group there are plenty of opportunities for students to join
in both on the masters and the PhD level. Corresponding theses focus on
the further development of Sherpa or its application to the simulation
of high-energy physics events at current and future collider
experiments.

So far we have spent a large amount of our time on the development of
the computer code which is written entirely in C++. Acquiring a fair
amount of working knowledge about this programming language is one of
the technical parts of any thesis so far.

Students with interest in particle physics phenomenology are invited to
contact the [Sherpa Team](SherpaTeam).

To gain some insight into previous work, take a look at the following:

PhD Theses
----------

-   [Monte-Carlo event generation for the
    LHC](http://etheses.dur.ac.uk/484/1/thesis.pdf) PhD thesis by Frank
    Siegert (2010)
-   [Perturbative QCD in Event
    Generation](http://www.freacafe.de/dnl/diss/diss_stefan.pdf) PhD
    thesis by Stefan Hoeche (2008)
-   [Simulation of signal and background processes for collider
    experiments](http://iktp.tu-dresden.de/IKTP/pub/07/diss_schumann.pdf)
    PhD thesis by Steffen Schumann (2008)
-   [QCD jet evolution at high and low
    scales](http://iktp.tu-dresden.de/IKTP/pub/07/diss_winter.pdf) PhD
    thesis by Jan Winter (2008)
-   [Automating methods to improve precision in Monte-Carlo event
    generation for particle
    colliders](http://iktp.tu-dresden.de/IKTP/pub/07/diss_gleisberg.pdf)
    PhD thesis by Tanju Gleisberg (2008)
-   [Event generation at hadron
    colliders](http://projects.hepforge.org/sherpa/theses/diss_schaelicke.pdf)
    PhD thesis by Andreas Schaelicke (2005)
-   [Event generation at lepton
    colliders](http://projects.hepforge.org/sherpa/theses/diss_kuhn.pdf)
    PhD thesis by Ralf Kuhn (2002)

Diploma Theses
--------------

-   Implementation of tau-lepton decays diploma thesis by Thomas
    Laubrich (2006)
-   Formation and fragmentation of hadronic clusters diploma thesis by
    Jan Winter (2003)
-   Helicity formalism for exotic physics scenarios at collider
    experiments diploma thesis by Tanju Gleisberg (2003)
-   Simulation of processes involving supersymmetry at high energy
    colliders diploma thesis by Steffen Schumann (2002)
-   Initial state radiation diploma thesis by Andreas Schaelicke (2001)
-   APACIC++, eine Partonkaskade in C++ diploma thesis by Ralf
    Kuhn (1998)
