.. _index:

Sherpa Homepage
==================================

Sherpa is a Monte Carlo event generator for the **S**\ imulation of
**H**\ igh-Energy **R**\ eactions of **PA**\ rticles in lepton-lepton,
lepton-photon, photon-photon, lepton-hadron and hadron-hadron
collisions. Simulation programs - also dubbed event generators - like
Sherpa are indispensable work horses for current particle physics
phenomenology and are (at) the interface between theory and
experiment.

- For a brief summary on the necessity and construction principles of
  event generators, see :ref:`monte-carlo-info`
- To download Sherpa, see :ref:`changelog`
- To browse the Sherpa manual online, see `the manual`_
- To find out more about the physics in Sherpa, see
  :ref:`publications` and :ref:`theses`.
- To get information about or contact the authors of Sherpa, see
  :ref:`SherpaTeam`
- To ask questions and browse answers about Sherpa, see
  `the Sherpa Issue Tracker`_
- To be informed about patches and newer releases, subscribe to our
  `announcement mailing list`_

.. toctree::
   :hidden:
   :maxdepth: 2
   :glob:

   monte-carlo
   changelog
   team
   publications
   theses
   pages/*

.. _the manual: https://sherpa-team.gitlab.io/sherpa/master/
.. _the Sherpa Issue Tracker: https://gitlab.com/sherpa-team/sherpa/issues/
.. _announcement mailing list: https://www.hepforge.org/lists/listinfo/sherpa-announce
